# CangjieAppDevelopment

本工程是**纯仓颉开发**的鸿蒙原生应用，主要参考 [HarmonyOS-Cases/Cases](https://gitee.com/harmonyos-cases/cases) 的案例集，提供 Cangjie 版本的实现。

应用截图如下：

<img src="./app_screen.gif">


## 项目结构

```bash
.
|-- AppScope/       # 应用的全局配置信息
|-- common/         # 公用模块目录
      |-- utils/        # utils 模块（Har）
|-- entry/          # 应用入口（Hap）
|-- feature/        # 业务模块目录
      |-- addressexchange/      # 1.地址交换动画案例（Har）
      |-- customtabbar/         # 2.自定义TabBar页签案例（Har）
      |-- functionalscenes/     # 功能模块（Har）
      |-- modalwindow/          # 3.全屏登录页面案例（Har）
      |-- pendingitems/         # 10.列表编辑实现案例（Har）
      |-- searchcomponent/      # 搜索模块（Har）
      |-- secondarylinkage/     # 9.二级联动实现案例（Har）
|-- hvigor/             # 构建引擎的配置文件
|-- build-profile.json5 # 工程级别的构建静态配置文件
|-- app_screen.gif      # 应用截图
|-- hvigorfile.ts       # 工程级别的构建动态自定义脚本
|-- oh-package.json5    # oh 包管理配置文件
|-- README.md           # 本文档
```

## 案例目录

#### 10.列表编辑实现案例

列表的编辑模式用途十分广泛，常见于待办事项管理、文件管理、备忘录的记录管理等应用场景。列表编辑实现方案可以参考此[详细说明文档](./feature/pendingitems/README.md)。

> 注意：仓颉 ListItem 的 .swipeAction 还有问题，无法左滑弹出图标。

<img src="./entry/src/main/resources/base/media/todo_list.gif" width="200">

### 9.二级联动实现案例

二级联动是指一个列表（一级列表）的选择结果，来更新另一个列表（二级列表）的选项。二级联动的方案可以参考此[详细说明文档](./feature/secondarylinkage/README.md)。

<img src="./entry/src/main/resources/base/media/secondary_linkage.gif">

### ~~8.阻塞事件冒泡案例【暂不支持】~~

仓颉缺少：

1. 触摸测试控制：`hitTestBehavior` 属性

期望效果：

<img src="./entry/src/main/resources/base/media/event_propagation.gif" width="200">

### ~~7.图片缩放效果实现案例【暂不支持】~~

仓颉缺少：

1. 手势处理的支持：包括`gesture` 属性及 `TapGesture`、`PinchGesture`、`PanGesture` 等接口；

期望效果：

<img src="./entry/src/main/resources/base/media/image_viewer.gif" width="200">

### ~~6.主页瀑布流实现案例【暂不支持】~~

仓颉缺少：

1. `WaterFlow` 组件

期望效果：

<img src="./entry/src/main/resources/base/media/functional_scenes.gif" width="200">


目前使用 List 组件替代，详情可编译、安装本 APP 体验。


### ~~5.滑动页面信息隐藏与组件位移效果案例【暂不支持】~~

仓颉缺少：

1. `SideBarContainer` 组件

期望效果：

<img src="./entry/src/main/resources/base/media/slide_to_hide_and_displace.gif" width="200">

### ~~4.MpChart图表实现案例【暂不支持】~~

仓颉缺少：

1. `MpChart` 三方库：[仓颉版的 MpChart 库](https://gitcode.com/Cangjie-TPC/chart4cj/overview)正在开发中；
2. `Radio` 组件

期望效果：

<img src="./entry/src/main/resources/base/media/bar_chart.gif" width="200">

### 3.全屏登录页面案例

本例介绍各种应用登录页面。在主页面点击跳转到全屏登录页后，显示全屏模态页面，全屏模态页面从下方滑出并覆盖整个屏幕，模态页面内容自定义，此处分为默认一键登录方式和其他登录方式。[详细说明文档](./feature/modalwindow/README.md)。

<img src="./entry/src/main/resources/base/media/modal_window.gif" width="200">

### 2.自定义TabBar页签案例

TabBar在大部分的APP当中都能够使用到，不同的APP可能存在不一样的TabBar样式，Tab组件自带的TabBar属性对于部分效果无法满足，如页签中间显示一圈圆弧外轮廓等，
因此我们需要去自己定义一个TabBar页签来满足开发的需要。自定义TabBar页签的方案可以参考此[详细说明文档](./feature/customtabbar/README.md)。

<img src="./entry/src/main/resources/base/media/custom_tabbar.gif" width="200">

### 1.地址交换动画案例

在出行类订票软件中，一般都有此动画效果，点击交换后，起点和终点互换。地址交换动画的方案可以参考此[详细说明文档](./feature/addressexchange/README.md)。

<img src="./entry/src/main/resources/base/media/address_exchange.gif" width="200">


