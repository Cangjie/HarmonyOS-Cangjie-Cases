# 仓颉开发 HarmonyOS NEXT 应用案例集

亲爱的仓颉开发者，您好！案例不定期更新，如果认为本项目对您有帮助，欢迎点击页面右上角的 Watch & Star，方便及时接收案例更新通知。

如果您对仓颉案例有任何诉求和建议，欢迎通过本项目的 Issue 告诉我们~~

## 项目介绍

本项目一方面参考 [HarmonyOS-Cases/Cases](https://gitee.com/harmonyos-cases/cases) 的案例集，另一方面基于合作伙伴及开发者们反馈的诉求和建议，提供一些使用仓颉开发鸿蒙原生应用的案例集，方便大家一起学习。

项目目录结构：

```bash
.
|-- ArkTSCangjieHybridApp/    # APP目录：ArkTS & Cangjie 混合工程示例
|-- CangjieAppDevelopment/    # APP目录：纯 Cangjie 工程示例
|-- README.md                 # 本文档
```

### ArkTSCangjieHybridApp 工程简介

ArkTSCangjieHybridApp 是一个 ArkTS & Cangjie 混合开发的工程，应用入口为 ArkTS，会调用到 ArktS/Cangjie 的模块（HAR），用于演示混合应用的开发。

具体请查看该目录下的 [README.md](./ArkTSCangjieHybridApp/README.md)。

应用截图如下（更多案例正在完善中）：

<img src="./ArkTSCangjieHybridApp/app_screen.gif">


### CangjieAppDevelopment 工程简介

CangjieAppDevelopment 是一个纯 Cangjie 开发的工程，应用入口为 Cangjie。

具体请查看该目录下的 [README.md](./CangjieAppDevelopment/README.md)。

应用截图如下（更多案例正在完善中）：

<img src="./CangjieAppDevelopment/app_screen.gif">

## 配套平台

下载地址：https://developer.huawei.com/consumer/cn/download/

### IDE

DevEco Studio NEXT Beta1(5.0.3.900)

### Cangjie SDK

DevEco Studio NEXT Beta1-Cangjie Plugin(5.0.3.900)
