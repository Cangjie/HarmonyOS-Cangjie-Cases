import { BaseDataSource } from "../model/BaseDataSource";
import { VideoData } from "../model/VideoData";
import { emptyVideoDrama, VideoDrama } from "../model/VideoDrama";
import { VideoPlayer } from "./VideoPlayer";

@Builder
export function VideoSwiperPageBuilder(name: string, param: Object) {
  VideoSwiperPage()
}

@Component
export struct VideoSwiperPage {
  pageInfos: NavPathStack = new NavPathStack();

  private swiperController: SwiperController = new SwiperController();
  @State curIndex: number = 0;
  @State firstFlag: boolean = true;

  @State videoDrama: VideoDrama = emptyVideoDrama()
  dataSource: BaseDataSource<VideoData> = new BaseDataSource<VideoData>(this.videoDrama.videoList)

  build() {
    NavDestination() {
      Column() {
        if (this.dataSource.totalCount() === 0) {
          Text('视频列表为空')
            .fontSize(20)
            .fontColor(Color.Gray)
            .fontWeight(FontWeight.Medium)
        }
        Stack({ alignContent: Alignment.TopStart }) {
          Swiper(this.swiperController) { // 使用Swiper组件实现滑动轮播显示的能力
            LazyForEach(this.dataSource, (item: VideoData, index: number) => {
              VideoPlayer({
                item: item,
                title: this.videoDrama.title,
                curIndex: this.curIndex,
                index: index,
                firstFlag: this.firstFlag
              })
            }, (item: VideoData) => item.url)
          }
          .index($$this.curIndex) // 设置当前在容器中显示的子组件的索引值
          .displayCount(1)
          .width('100%')
          .height('100%')
          .autoPlay(false)
          .indicator(false)
          .loop(true)
          .duration(200) // 子组件切换的动画时长
          .cachedCount(1)
          .vertical(true)
          .itemSpace(0)

          // 左箭头 + 第x集
          Row({ space: 10 }) {
            Image($r('app.media.shortvideo_left_arrow'))
              .width(30)
              .height(30)
              .onClick(() => {
                console.log('tjl > click back')
                this.pageInfos.pop()
              })
            Text(`第${this.curIndex + 1}集`)
              .fontColor(Color.White)
              .fontSize(20)
          }
          .margin({ top: 30, left: 10 })
        }
      }
    }
    .title("视频播放页")
    .hideTitleBar(true)
    .onReady((context: NavDestinationContext) => {
      this.pageInfos = context.pathStack
      console.info('current page config info is ' + JSON.stringify(context.getConfigInRouteMap()));
      console.info('current page param is ' + JSON.stringify(this.pageInfos.getParamByName('shortvideoarkts/VideoSwiperPage')));
      this.videoDrama = this.pageInfos.getParamByName('shortvideoarkts/VideoSwiperPage')[0] as VideoDrama
      this.dataSource.pushList(this.videoDrama.videoList)
    })
  }
}