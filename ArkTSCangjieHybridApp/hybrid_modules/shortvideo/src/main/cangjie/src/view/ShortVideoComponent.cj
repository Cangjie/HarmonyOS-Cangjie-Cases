package ohos_app_cangjie_shortvideo.view

import ohos.base.*
import ohos.hybrid_base.*
import ohos.component.*
import ohos.state_manage.*
import ohos.state_macro_manage.*

import cj_res_shortvideo.*
import ohos_app_cangjie_shortvideo.model.*
import ohos_app_cangjie_shortvideo.data.*
import ohos_app_cangjie_shortvideo.*

@HybridComponentEntry
@Component
class ShortVideoComponent {
    @LocalStorageLink["index", "InterOp"] var index: Int64 = 0

    let dataSource: VideoDramaSource = VideoDramaSource(videoDramaList)

    public func build() {
        Grid() {
            LazyForEach(
                this.dataSource,
                itemGeneratorFunc: { item: VideoDrama, index: Int64 =>
                    GridItem() {
                        Column(5) {
                            Image(item.cover)
                                .width(100.percent)
                                .height(100.percent)
                                .borderRadius(10)
                                .objectFit(ImageFit.Contain)
                                .foregroundColor(Color.GRAY)

                            Text(item.title)
                                .fontSize(14)
                                .fontColor(Color.BLACK)
                                .fontWeight(FontWeight.Medium)
                                .textOverflow(TextOverflow.Ellipsis)
                                .maxLines(1)

                            Row(10) {
                                Text(item.category)
                                    .fontSize(12)
                                    .fontColor(Color.GRAY)
                                Text("${item.videoList.size}集")
                                    .fontSize(12)
                                    .fontColor(Color.GRAY)
                            }
                            .justifyContent(FlexAlign.Start)
                        }
                        .alignItems(HorizontalAlign.Start)
                        .padding(10)
                    }
                    .onClick({ event =>
                        AppLog.info("[CJ ShortVideoComponent] onClick")
                        // 通过互操作调用 JS 注册的 RouterToVideoSwiperPage 方法，实现页面跳转
                        this.index = index  // FIXME：但由于 ArkTS 页面无法与仓颉组件进行传参，只能临时通过全局变量进行配置。
                        if (let Some(fn) <- g_jsFunctionMap.get("RouterToVideoSwiperPage")) {
                            AppLog.info("[CJ ShortVideoComponent] call jsfunction: RouterToVideoSwiperPage")
                            fn.call()
                        }
                    })
                },
                keyGeneratorFunc: { item: VideoDrama, index: Int64 =>
                    return item.title + item.videoList.size.toString()
                })
        }
        .columnsTemplate("1fr 1fr 1fr")
        .rowsTemplate("1fr 1fr 1fr 1fr")
        .width(100.percent)
        .height(100.percent)
    }
}
