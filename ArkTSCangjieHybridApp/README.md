# ArkTSCangjieHybridApp

本工程是 **ArkTS & Cangjie 混合开发**的鸿蒙原生应用，基于合作伙伴及开发者们反馈的诉求和建议，提供一些 ArkTS & Cangjie 混合开发的案例集。

应用截图如下：

<img src="./app_screen.gif">

## 项目结构

```bash
.
|-- AppScope/           # 应用的全局配置信息
|-- arkts_modules/      # 纯 ArkTS 开发的模块目录
      |-- shortvideoarkts   # 七猫短视频Demo（ArkTS版本）（Har）
|-- entry/              # 应用入口（Hap）
|-- hvigor/             # 构建引擎的配置文件
|-- hybrid_modules/     # ArkTS + Cangjie 混合开发的模块目录
      |-- shortvideo        # 七猫短视频Demo（Cangjie版本）（Har）
|-- build-profile.json5 # 工程级别的构建静态配置文件
|-- app_screen.gif      # 应用截图
|-- hvigorfile.ts       # 工程级别的构建动态自定义脚本
|-- oh-package.json5    # oh 包管理配置文件
|-- README.md           # 本文档
```

## 案例目录


### 1. 七猫短视频Demo

实现了一个短剧列表页，点击某个短剧后，会跳转到播放器页面，可以通过上滑、下滑切换视频。并且实现了倍速播放、进度条拖动等功能。

<img src="./entry/src/main/resources/base/media/short_video.gif">


